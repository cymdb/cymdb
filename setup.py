# -*- coding: utf-8 -*-

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

try:
    import pkgconfig
    libmdb_flags = pkgconfig.parse('libmdb')
    # cythonize needs all the flags to be lists not sets
    libmdb_flags = {key: list(value) for key, value in libmdb_flags.items()}
        
except ImportError:
    libmdb_flags = {'libraries': ['mdb', 'glib-2.0'],
                    'include_dirs': ['/usr/include/glib-2.0',
                                     '/usr/lib64/glib-2.0/include'],
                    'define_macros': []}


extensions=[
    Extension("cymdb", ["cymdb.pyx"],
              **libmdb_flags)]


setup(
    name='cymdb',
    version='0.0.1a1',
    description = "Cython bindings for MDBtools",
    ext_modules = cythonize(extensions)
)
