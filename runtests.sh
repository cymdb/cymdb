#!/bin/sh

mkdir -p build
rm -rf build/testenv
rm -f *.c *.so
virtualenv --system-site-packages build/testenv
source build/testenv/bin/activate

pip install --editable .

nosetests
