"""
Cython interface definition file for mdbtools.h
"""

# This file is a derivative work of mdbtools, which is released under the GNU
# General Public License version 2 or greater.
# This file is therefore also released under the same licence.
__license__ = "GPLv2+"

from glib cimport gboolean, gchar, GPtrArray


cdef extern from "mdbtools.h":
    
    ## #define constants
    cdef enum:
        MDB_MAX_OBJ_NAME = 256
        MDB_PGSIZE = 4096
        MDB_CATALOG_PG = 18
        MDB_MEMO_OVERHEAD = 12
        MDB_BIND_SIZE = 16384
    ## need a separate enum for MDB_MAX_COLS since it equals MDB_MAX_OBJ_NAME
    cdef enum:
        MDB_MAX_COLS = 256
        MDB_MAX_IDX_COLS = 10
    
    ctypedef enum MdbFileFlags:
        MDB_NOFLAGS = 0x00
        MDB_WRITABLE = 0x01
    
    ## Catalog entry types
    cdef enum:
        MDB_FORM = 0
        MDB_TABLE
        MDB_MACRO
        MDB_SYSTEM_TABLE
        MDB_REPORT
        MDB_QUERY
        MDB_LINKED_TABLE
        MDB_MODULE
        MDB_RELATIONSHIP
        MDB_UNKNOWN_09
        MDB_UNKNOWN_0A
        MDB_DATABASE_PROPERTY
        MDB_ANY = -1
    
    ## Column types
    cdef enum:
        MDB_BOOL = 0x01
        MDB_BYTE = 0x02
        MDB_INT = 0x03
        MDB_LONGINT = 0x04
        MDB_MONEY = 0x05
        MDB_FLOAT = 0x06
        MDB_DOUBLE = 0x07
        MDB_DATETIME = 0x08
        MDB_BINARY = 0x09
        MDB_TEXT = 0x0a
        MDB_OLE = 0x0b
        MDB_MEMO = 0x0c
        MDB_REPID = 0x0f
        MDB_NUMERIC = 0x10
        MDB_COMPLEX = 0x12
    
    ## Data structures
    ctypedef struct MdbFile:
        int fd
        gboolean writable
        char *filename
        # ...

    ctypedef struct MdbHandle:
        MdbFile *f
        # ...
        unsigned int  num_catalog
        GPtrArray *catalog
        # ...

    ctypedef struct MdbCatalogEntry:
        MdbHandle *mdb
        char object_name[MDB_MAX_OBJ_NAME+1]
        int object_type
        # ...

    ctypedef struct MdbTableDef:
        MdbCatalogEntry *entry
        char name[MDB_MAX_OBJ_NAME+1]
        unsigned int num_cols
        GPtrArray *columns
        unsigned int num_rows
        # index stuff skipped
        unsigned int cur_row
        # lots more stuff
    
    ctypedef struct MdbColumn:
        MdbTableDef *table
        char name[MDB_MAX_OBJ_NAME+1]
        int col_type
        int col_size
        int col_num
        ## N.B. calling mdb_read_row fills bind_ptr with sprintf(value)
        void *bind_ptr
        int *len_ptr
        # lots of other stuff...

    ctypedef struct MdbField:
        void *value
        int siz
        int start
        unsigned char is_null
        unsigned char is_fixed
        int colnum
        int offset

    # from file.c
    MdbHandle* mdb_open(const char *filename, MdbFileFlags flags)
    void mdb_close(MdbHandle *mdb)
    MdbHandle *mdb_clone_handle(MdbHandle *mdb)
    # from catalog.c
    void mdb_free_catalog(MdbHandle *mdb)
    GPtrArray *mdb_read_catalog(MdbHandle *mdb, int obj_type)
    MdbCatalogEntry *mdb_get_catalogentry_by_name(MdbHandle *mdb, const gchar* name)
    # from table.c
    int mdb_is_user_table(MdbCatalogEntry *entry)
    void mdb_free_tabledef(MdbTableDef *table)
    MdbTableDef *mdb_read_table(MdbCatalogEntry *entry)
    GPtrArray *mdb_read_columns(MdbTableDef *table)
    # from data.c
    int mdb_rewind_table(MdbTableDef *table)
    int mdb_fetch_row(MdbTableDef *table)
    int mdb_find_row(MdbHandle *mdb, int row, int *start, size_t *len)
    # from write.c
    int mdb_crack_row(MdbTableDef *table, int row_start, int row_end, MdbField *fields)
