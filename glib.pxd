"""
Cython interface definition file for glib.h
"""

# This file is a derivative work of glib, which is released under the GNU
# Library General Public License version 2 or greater.
# This file is therefore also released under the same licence.
__license__ = "LGPLv2+"

cdef extern from "glib.h":

    ctypedef char gchar
    ctypedef short gshort
    ctypedef long glong
    ctypedef int gint
    ctypedef gint gboolean
    ctypedef unsigned char guchar
    ctypedef unsigned short gushort
    ctypedef unsigned long gulong
    ctypedef unsigned int guint
    ctypedef float gfloat
    ctypedef double gdouble
    ctypedef void *gpointer
    ctypedef const void *gconstpointer

    cdef struct _GPtrArray:
        gpointer *pdata
        guint len
    ctypedef _GPtrArray GPtrArray

    gpointer g_ptr_array_index (GPtrArray *array, guint index)
