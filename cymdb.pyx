"""
Main cython file with the class interface to libmdb
"""

__author__ = "Chris Kerr"
__copyright__ = "Copyright Chris Kerr 2014"
__license__ = "GPL version 3+"

# This file is part of cymdb.
#
# cymdb is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# cymdb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cymdb.  If not, see <http://www.gnu.org/licenses/>.

# For some reason passing pointers to class __cinit__ functions does not work,
# so we cast them to uintptr_t and cast back afterwards
from libc.stdint cimport uintptr_t
from libc.stdlib cimport malloc, free


import sys

import numpy as np

from glib cimport GPtrArray, g_ptr_array_index

from mdbtools cimport *

## pre-declaring classes
cdef class Row
cdef class Column
cdef class Table
cdef class CatalogEntry
cdef class MDBfile


cdef ensure_str(text, encoding='ascii'):
    if isinstance(text, str):
        return text
    elif sys.version_info.major >= 3:
        return str(text, encoding)
    else:
        return str(text)


cdef ensure_bytes(text, encoding='ascii'):
    if isinstance(text, bytes):
        return text
    elif sys.version_info.major >= 3:
        return bytes(text, encoding)
    else:
        return bytes(text)


cdef ensure_unicode(text, encoding='utf16'):
    # Create a 'unicode_type' so as not to clobber the global name 'unicode'
    if sys.version_info.major >= 3:
        unicode_type = str  # unicode is not defined in python 3
    else:
        unicode_type = unicode
    if isinstance(text, unicode_type):
        return text
    elif isinstance(text, bytes):
        return text.decode(encoding=encoding)
    else:
        return unicode_type(text)


cdef class Row:
    """Wrapper for MdbField array"""

    cdef MdbField* _impl
    cdef size_t nfields

    def __cinit__(self, uintptr_t fields, size_t nfields):
        self._impl = <MdbField*>fields
        self.nfields = nfields

    def __dealloc__(self):
        free(self._impl)

    def __len__(self):
        return self.nfields

    def __getitem__(self, i):
        return <uintptr_t>(self._impl) + i * sizeof(MdbField)


cdef class Column:
    """Wrapper for MdbColumn"""
    
    cdef MdbColumn* _impl
    
    def __cinit__(self, uintptr_t col):
        self._impl = <MdbColumn*>col
    
    property name:
        def __get__(self):
            return ensure_str(self._impl.name)
    
    property col_type:
        def __get__(self):
            if self._impl.col_type == MDB_BOOL:
                return np.bool_
            elif self._impl.col_type == MDB_BYTE:
                return np.dtype('i1')
            elif self._impl.col_type == MDB_INT:
                return np.dtype('<i2')
            elif self._impl.col_type == MDB_LONGINT:
                return np.dtype('<i4')
            elif self._impl.col_type == MDB_FLOAT:
                return np.dtype('<f4')
            elif self._impl.col_type == MDB_DOUBLE:
                return np.dtype('<f8')
            elif self._impl.col_type == MDB_BINARY:
                return bytes
            elif self._impl.col_type == MDB_TEXT:
                return unicode
            elif self._impl.col_type == MDB_DATETIME:
                raise NotImplementedError
            elif self._impl.col_type == MDB_MEMO:
                raise NotImplementedError
            elif self._impl.col_type == MDB_MONEY:
                raise NotImplementedError
            elif self._impl.col_type == MDB_NUMERIC:
                raise NotImplementedError
            elif self._impl.col_type == MDB_REPID:
                raise NotImplementedError
            else:
                raise ValueError("Invalid col_type: %d" % self._impl.col_type)

    def parse_data(self, uintptr_t field_ptr):
        cdef MdbField* field = <MdbField*>field_ptr
        cdef bytes field_data
        if self._impl.col_type == MDB_BOOL:
            assert field.siz == 0
            return np.bool_(not field.is_null)
        else:
            field_data = (<char*>field.value)[:field.siz]
            if self._impl.col_type == MDB_BINARY:
                return field_data
            elif self._impl.col_type == MDB_TEXT:
                return ensure_unicode(field_data, encoding='utf16')
            else:
                # not the most efficient way of doing this, I know
                dtype = self.col_type
                assert field.siz == dtype.itemsize
                return np.fromstring(field_data, dtype=dtype)

    def __dealloc__(self):
        # N.B. the MdbColumn object belongs to the MdbTableDef and is freed
        # when mdb_free_tabledef is called
        pass


cdef class Table:
    """Wrapper for MdbTableDef"""
    
    cdef MdbTableDef* _impl
    
    def __cinit__(self, uintptr_t tabledef):
        cdef GPtrArray* col_array
        self._impl = <MdbTableDef*>tabledef
        col_array = mdb_read_columns(self._impl)
        if col_array is NULL:
            raise MemoryError("mdb_read_columns returned NULL")
        mdb_rewind_table(self._impl)  # always returns 0
    
    property name:
        def __get__(self):
            return ensure_str(self._impl.name)
    
    property num_cols:
        def __get__(self):
            return self._impl.num_cols
    
    property num_rows:
        def __get__(self):
            return self._impl.num_rows
    
    property cur_row:
        def __get__(self):
            return self._impl.cur_row


    def iter_columns(self):
        """Iterate over all columns"""
        for i in range(self._impl.num_cols):
            yield Column(<uintptr_t>g_ptr_array_index(self._impl.columns, i))


    def get_column(self, key):
        """Get a column by index or name"""
        cdef MdbColumn *col_ptr
        if isinstance(key, int):
            if (key < 0 or key >= self._impl.num_cols):
                raise IndexError('Index %d out of range' % key)
            col_ptr = <MdbColumn*> g_ptr_array_index(self._impl.columns, key)
        elif isinstance(key, (bytes, str)):
            key = ensure_bytes(key)
            for i in range(self._impl.num_cols):
                col_ptr = <MdbColumn*> g_ptr_array_index(self._impl.columns, i)
                if col_ptr.name == key:
                    break
            else:
                raise KeyError("No column named %s" % key)
        else:
            raise TypeError("Key can only be int, bytes or str")
        return Column(<uintptr_t>col_ptr)

    def fetch_row(self):
        """Read the next row in the table"""
        cdef int result
        # TODO check for self._impl.num_rows == 0 and handle sensibly
        # (raise StopIteration?)
        result = mdb_fetch_row(self._impl)
        if result != 1:
            raise RuntimeError("mdb_fetch_row failed")
        # mdb_read_row fills the bound pointers with text representations of
        # the data, but there is no way of getting at the binary data directly,
        # so we have to reimplement the internals of mdb_read_row here
        cdef int row_start, row_end
        cdef size_t row_size
        # mdb_fetch_row increments cur_row, we want to read the previously
        # read row again
        result = mdb_find_row(self._impl.entry.mdb, self._impl.cur_row - 1,
                              &row_start, &row_size)
        if result != 0:
            raise RuntimeError("mdb_find_row failed")
        row_end = row_start + row_size - 1

        cdef int nfields
        cdef MdbField* fields
        # TODO see if a smaller array than MDB_MAX_COLS can be used
        # e.g. self._impl.num_cols
        # N.B. mdbtools uses MDB_MAX_COLS internally
        fields = <MdbField*>malloc(MDB_MAX_COLS * sizeof(MdbField))
        nfields = mdb_crack_row(self._impl, row_start, row_end, fields)
        return Row(<uintptr_t>fields, nfields)
        # Row.__dealloc__ handles freeing of fields array

    def __iter__(self):
        cdef int i, j
        columns = tuple(self.iter_columns())
        for i in range(self._impl.num_rows):
            row = self.fetch_row()
            row_list = list()
            # print(len(row), self.num_cols)
            # assert len(row) == self.num_cols
            for j in range(self.num_cols):
                row_list.append(columns[j].parse_data(row[j]))
            yield tuple(row_list)

    def read_numpy_array(self):
        columns = tuple(self.iter_columns())
        dtype_list = [(col.name, col.col_type) for col in columns]
        dtype = np.dtype(dtype_list)
        arr = np.empty(shape=(self.num_rows,), dtype=dtype)
        for i in range(self._impl.num_rows):
            row = self.fetch_row()
            for j, col in enumerate(columns):
                arr[i][col.name] = col.parse_data(row[j])
        return arr

    def __dealloc__(self):
        # mdb_free_tabledef checks for NULL pointer and will free all sub
        # arrays such as the columns array
        mdb_free_tabledef(self._impl)


cdef class CatalogEntry:
    """Wrapper for MdbCatalogEntry"""

    cdef MdbCatalogEntry* _impl

    def __cinit__(self, uintptr_t entry):
        self._impl = <MdbCatalogEntry*>entry

    property name:
        def __get__(self):
            return ensure_str(self._impl.object_name)

    def is_user_table(self):
        return bool(mdb_is_user_table(self._impl))

    def is_system_table(self):
        return not self.is_user_table()

    def read_table(self):
        cdef MdbTableDef* table
        table = mdb_read_table(self._impl)
        if table is NULL:
            raise MemoryError('mdb_read_table returned NULL')
        return Table(<uintptr_t>table)

    def __dealloc__(self):
        # The MdbCatalogEntry object is owned by the GPtrArray, which is in
        # turn owned by the MdbHandle. i.e. don't free it here...
        pass


cdef class MDBfile:
    """Wrapper class for MdbHandle"""
    
    cdef MdbHandle* handle
    
    def __cinit__(self, filename, MdbFileFlags flags=MDB_NOFLAGS):
        
        filename = ensure_bytes(filename, encoding='utf-8')
            
        self.handle = mdb_open(filename, flags)
        if self.handle is NULL:
            raise MemoryError("mdb_open returned NULL")

        # Read the whole catalog by default
        self._read_catalog()

    def _read_catalog(self, int objtype=MDB_ANY):
        """Read the list of tables

        Note that this function will invalidate any CatalogEntry objects
        created from the previous catalog."""
        cdef GPtrArray* catalog
        # N.B. mdb_read_catalog frees the old catalog array if it exists
        catalog = mdb_read_catalog(self.handle, objtype)
        if catalog is NULL:
            raise MemoryError("mdb_read_catalog returned NULL")

    def get_catalog_entry(self, key):
        """Get a catalog entry by index or name"""
        cdef MdbCatalogEntry* entry_ptr
        if isinstance(key, int):
            if (key < 0 or key >= self.handle.catalog.len):
                raise IndexError('Index %d out of range' % key)
            entry_ptr = <MdbCatalogEntry*> g_ptr_array_index(self.handle.catalog, key)
        elif isinstance(key, (bytes, str)):
            key = ensure_bytes(key)
            entry_ptr = mdb_get_catalogentry_by_name(self.handle, key)
            if entry_ptr is NULL:
                raise KeyError("key %s not in catalog" % key)
        else:
            raise TypeError("Key can only be int, bytes or str")

        return CatalogEntry(<uintptr_t>entry_ptr)

    def __getitem__(self, key):
        entry = self.get_catalog_entry(key)
        return entry.read_table()

    def __iter__(self):
        """Iterate through the catalog entries"""
        for i in range(self.handle.catalog.len):
            entry = CatalogEntry(<uintptr_t>g_ptr_array_index(self.handle.catalog, i))
            if entry.is_user_table():
                yield entry.read_table()

    def close(self):
        mdb_free_catalog(self.handle)
        mdb_close(self.handle)
        self.handle = NULL

    ## context manager stuff
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):        
        if self.handle is not NULL:
            self.close()

    ## Cython destructor stuff
    def __dealloc__(self):
        # mdb_close checks for NULL pointer
        mdb_close(self.handle)
        