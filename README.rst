cymdb
=====

An object-oriented wrapper for mdbtools_ using cython_

.. _mdbtools: http://mdbtools.sourceforge.net/
.. _cython: http://cython.org


Installing
----------

You first of all need to have mdbtools and cython installed.
The pkgconfig_ package will help if you have mdbtools installed in an
unusual location.

.. _pkgconfig: https://github.com/matze/pkgconfig


Alternatives
------------

- pymdb (https://pypi.python.org/pypi/pymdb/)
    A lower-level python interface to the mdbtools library
