# -*- coding: utf-8 -*-

import sys
import os.path

import numpy as np
from numpy.testing.utils import assert_array_almost_equal, assert_array_equal
from nose.tools import assert_equal, assert_in

import cymdb


def data_file(filename):
    return os.path.join('testdata', filename)


def test_open_close():
    mdb = cymdb.MDBfile(data_file('arbin1.res'))
    mdb.close()


def test_context_manager():
    with cymdb.MDBfile(data_file('arbin1.res')):
        pass


def test_catalog_number():
    with cymdb.MDBfile(data_file('arbin1.res')) as mdb:
        cat1 = mdb.get_catalog_entry(1)
        assert_equal(cat1.name, 'Databases')
        assert cat1.is_system_table()


def test_catalog_name():
    with cymdb.MDBfile(data_file('arbin1.res')) as mdb:
        global_table = mdb.get_catalog_entry('Global_Table')
        assert_equal(global_table.name, 'Global_Table')
        assert global_table.is_user_table()


def test_iter_tables():
    with cymdb.MDBfile(data_file('arbin1.res')) as mdb:
        for table in mdb:
            pass


def test_read_table():
    with cymdb.MDBfile(data_file('arbin1.res')) as mdb:
        table = mdb['Version_Table']
        assert_equal(table.name, 'Version_Table')


def test_table_properties():
    with cymdb.MDBfile(data_file('arbin1.res')) as mdb:
        table = mdb['Channel_Normal_Table']
        assert_equal(table.num_cols, 18)
        assert_equal(table.num_rows, 2633)


def test_get_column_by_number():
    with cymdb.MDBfile(data_file('arbin1.res')) as mdb:
        table = mdb['Global_Table']
        col0 = table.get_column(0)
        assert_equal(col0.name, 'Test_ID')
        assert_equal(col0.col_type, np.dtype('<i4'))


def test_get_column_by_name():
    # Create a 'unicode_type' so as not to clobber the global name 'unicode'
    if sys.version_info.major >= 3:
        unicode_type = str  # unicode is not defined in python 3
    else:
        unicode_type = unicode
    with cymdb.MDBfile(data_file('arbin1.res')) as mdb:
        table = mdb['Global_Table']
        comments = table.get_column('Comments')
        assert_equal(comments.name, 'Comments')
        assert_equal(comments.col_type, unicode_type)


def test_iter_columns():
    with cymdb.MDBfile(data_file('arbin1.res')) as mdb:
        table = mdb['Channel_Normal_Table']
        for col in table.iter_columns():
            assert_in(col.col_type, (np.int8, np.int16, np.int32,
                                     np.float32, np.float64))

def test_iter_rows():
    with cymdb.MDBfile(data_file('arbin1.res')) as mdb:
        table = mdb['Channel_Normal_Table']
        for row in table:
            pass


def test_read_data():
    with cymdb.MDBfile(data_file('arbin1.res')) as mdb:
        table = mdb['Global_Table']
        table_list = list(iter(table))
        assert_equal(len(table_list), 1)
        row = table_list[0]
        assert_equal(row[0], 1)
        assert_equal(row[1], u"Chris_2011-01-27_LiSi_1-01-001")

def test_read_array():
    with cymdb.MDBfile(data_file('arbin1.res')) as mdb:
        table = mdb['Channel_Normal_Table']
        arr = table.read_numpy_array()
        csv_arr = np.loadtxt(data_file('arbin1-ChannelNormal-export.csv'),
                             skiprows=1)
        #assert_array_equal(arr['Data_Point'], csv_arr[:, 1])
        assert_array_almost_equal(arr['Test_Time'], csv_arr[:, 2])
        